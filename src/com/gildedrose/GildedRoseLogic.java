package com.gildedrose;

public class GildedRoseLogic {

	public static void concertUpdater(Item concert) {
		concert.lowerSellIn();
		
		if (concert.getQuality() < 50) {
			concert.riseQuality();
		}
		if (concert.getSellIn() < 11) {
			concert.riseQuality();
		}
		if (concert.getSellIn() < 0) {
			concert.setQuality(0);
		}
	}
	
	public static void agedBrieUpdater(Item agedBrie) {
		agedBrie.lowerSellIn();
		
		if (agedBrie.getQuality() < 50) {
			agedBrie.riseQuality();
		}
		if (agedBrie.getSellIn() < 0 && agedBrie.getQuality() < 50) {
			agedBrie.riseQuality();
		}
	}
	
	public static void sulfurasUpdater(Item sulfuras) {
		// nothing to do
	}
	
	public static void otherItemUpdater(Item other) {
		other.lowerSellIn();
		
		if (other.getQuality() > 0) {
			other.lowerQuality();
			other.lowerQuality();
		}
	}
}
