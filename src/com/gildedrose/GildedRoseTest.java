package com.gildedrose;

import org.junit.Assert;
import org.junit.Test;

public class GildedRoseTest {
	
	private static final int LOW_SELL_IN = -1;
	private static final int HIGH_SELL_IN = 12;
	private static final int LOW_QUALITY = 0;
	private static final int HIGH_QUALITY = 49;
	private static final int VERY_HIGH_QUALITY = 51;

	@Test
	public void should_lower_sell_in_and_rise_quality_twice_when_aged_brie_with_low_sell_in_low_quality() {
		Item[] items = new Item[] { new Item(Item.AGED_BRIE, LOW_SELL_IN, LOW_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(LOW_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(LOW_QUALITY + 2, items[0].getQuality());
	}
	
	@Test
	public void should_lower_sell_in_and_rise_quality_when_aged_brie_with_low_sell_in_high_quality() {
		Item[] items = new Item[] { new Item(Item.AGED_BRIE, LOW_SELL_IN, HIGH_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(LOW_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(HIGH_QUALITY + 1, items[0].getQuality());
	}
	
	@Test
	public void should_lower_sell_in_without_changing_quality_when_aged_brie_with_high_sell_in_very_high_quality() {
		Item[] items = new Item[] { new Item(Item.AGED_BRIE, HIGH_SELL_IN, VERY_HIGH_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(HIGH_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(VERY_HIGH_QUALITY, items[0].getQuality());
	}
	
	@Test
	public void should_lower_sell_in_and_set_quality_to_zero_when_concert_with_low_sell_in_low_quality() {
		Item[] items = new Item[] { new Item(Item.CONCERT, LOW_SELL_IN, LOW_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(LOW_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(0, items[0].getQuality());
	}
	
	@Test
	public void should_lower_sell_in_and_set_quality_to_zero_when_concert_with_low_sell_in_high_quality() {
		Item[] items = new Item[] { new Item(Item.CONCERT, LOW_SELL_IN, HIGH_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(LOW_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(0, items[0].getQuality());
	}
	
	@Test
	public void should_lower_sell_in_and_rise_quality_when_concert_with_high_sell_in_high_quality() {
		Item[] items = new Item[] { new Item(Item.CONCERT, HIGH_SELL_IN, HIGH_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(HIGH_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(HIGH_QUALITY + 1, items[0].getQuality());
	}
	
	@Test
	public void should_lower_sell_in_without_changing_quality_when_concert_with_high_sell_in_very_high_quality() {
		Item[] items = new Item[] { new Item(Item.CONCERT, HIGH_SELL_IN, VERY_HIGH_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(HIGH_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(VERY_HIGH_QUALITY, items[0].getQuality());
	}
	
	@Test
	public void should_not_change_when_sulfuras_with_low_sell_in_high_quality() {
		Item[] items = new Item[] { new Item(Item.SULFURAS, LOW_SELL_IN, HIGH_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(LOW_SELL_IN, items[0].getSellIn());
		Assert.assertEquals(HIGH_QUALITY, items[0].getQuality());
	}

	@Test
	public void should_lower_sell_in_without_changing_quality_when_elixir_with_low_sell_in_low_quality() {
		Item[] items = new Item[] { new Item(Item.ELIXIR, LOW_SELL_IN, LOW_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(LOW_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(LOW_QUALITY, items[0].getQuality());
	}
	
	@Test
	public void should_lower_sell_in_and_lower_quality_twice_when_elixir_with_low_sell_in_high_quality() {
		Item[] items = new Item[] { new Item(Item.ELIXIR, LOW_SELL_IN, HIGH_QUALITY) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		Assert.assertEquals(LOW_SELL_IN - 1, items[0].getSellIn());
		Assert.assertEquals(HIGH_QUALITY - 2, items[0].getQuality());
	}
}
