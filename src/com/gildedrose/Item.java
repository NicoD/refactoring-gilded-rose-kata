package com.gildedrose;

public class Item {
	
	public static final String CONCERT = "Backstage passes to a TAFKAL80ETC concert";
	public static final String AGED_BRIE = "Aged Brie";
	public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
	public static final String ELIXIR = "Elixir of the Mongoose";

	private String name;
	private int sellIn;
	private int quality;

	public Item(String name, int sellIn, int quality) {
		this.name = name;
		this.sellIn = sellIn;
		this.quality = quality;
	}
	
	public String getName() {
		return name;
	}

	public int getSellIn() {
		return sellIn;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}
	
	public void lowerSellIn() {
		this.sellIn --;
	}
	
	public void riseQuality() {
		this.quality ++;
	}
	
	public void lowerQuality() {
		this.quality --;
	}
}
