package com.gildedrose;

class GildedRose {
	Item[] items;

	public GildedRose(Item[] items) {
		this.items = items;
	}

	public void updateQuality() {
		for (int i = 0; i < items.length; i++) {
			if (Item.AGED_BRIE.equals(items[i].getName())) {
				GildedRoseLogic.agedBrieUpdater(items[i]);
			}
			if (Item.CONCERT.equals(items[i].getName())) {
				GildedRoseLogic.concertUpdater(items[i]);
			}
			if (Item.SULFURAS.equals(items[i].getName())) {
				GildedRoseLogic.sulfurasUpdater(items[i]);
			}
			if (Item.ELIXIR.equals(items[i].getName())) {
				GildedRoseLogic.otherItemUpdater(items[i]);
			}
		}
	}
}
